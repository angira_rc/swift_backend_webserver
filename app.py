from flask import Flask, request, session, jsonify, abort, url_for, g
from flask_sqlalchemy import SQLAlchemy
from passlib.apps import custom_app_context as pwd_context
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS, cross_origin
from datetime import datetime
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
import directory as dr
from status import Status
import socket
import requests

app = Flask(__name__)
cors = CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://angira:opend00r@localhost/Swift'
app.config['CORS_HEADERS'] = 'Content-Type'
db = SQLAlchemy(app)
auth = HTTPBasicAuth()

hosts = ['192.168.0.135', '127.0.0.1']

@auth.verify_password
def verify_password(username_or_token, password):
    user = User.verify_auth_token(username_or_token)
    if not user:
        user = User.query.filter_by(uName = username_or_token).first()
        if not user or not user.verify_password(password):
            return False

    g.user = user
    return True

@app.route('/api', methods = ['GET'])
def active ():
    # Set git remote here
    return jsonify({ 'OK': True })

@app.route('/api/login', methods = ['POST'])
def login ():
    req = request.get_json()
    print(req)
    uname = req['uname']
    pwd = req['pwd']

    if uname is None or pwd is None:
        abort(400)
        
    if verify_password(uname, pwd):
        token = g.user.generate_auth_token()
        return jsonify(Status(True, token.decode('ascii')).json())

    abort(404)

@app.route('/api/user', methods = ['POST'])
def createUser ():
    req = request.get_json()
    fname = req['fName']
    lname = req['lName']
    uname = req['uName']
    email = req['email']
    phone = req['phone']
    pwd = req['pwd']

    if fname is None or lname is None or uname is None or email is None or phone is None or pwd is None:
        abort(401)
    
    user = User(fName = fname, lName = lname, uName = uname, email = email, phone = phone)
    user.hash_password(pwd)

    db.session.add(user)
    db.session.commit()

    data = { 'username': uname }
    url = f'http://{who_is_online()}:5001/dist/user'
    
    requests.post(url = url, json = data)

    op = Operation(name = f"Create User Account for {user.uName}", typeOf = "Initial", user = user.id)
    db.session.add(op)
    db.session.commit()

    return jsonify({ 'username': user.uName }), 201, {'Location': url_for('createUser', id = user.id, _external = True)}

@app.route('/api/user/get', methods = ['GET'])
@auth.login_required
def user():
    info = {
        'fname': g.user.fName,
        'lname': g.user.lName,
        'uname': g.user.uName,
        'email': g.user.email,
        'phone': g.user.phone
    }
    usr = Status(True, info).json()
    return jsonify(usr)

@app.route('/api/files/hierarchy', methods = ['GET'])
@auth.login_required
def hierarchy():
    data = { 'username': g.user.uName }
    url = f'http://{who_is_online()}:5001/dist/files/hierarchy'
    
    hierarchy = requests.post(url = url, json = data)

    return jsonify(hierarchy.json())

@app.route('/api/files/read', methods = ['POST'])
@auth.login_required
def read ():
    req = request.get_json()
    
    data = {
        'username': g.user.uName,
        'path': req['path'],
        'name': req['name']
    }
    url = f'http://{who_is_online()}:5001/dist/files/read'
    
    content = requests.post(url = url, json = data)
    return jsonify(content.json())

@app.route('/api/files', methods = ['POST', 'PUT', 'DELETE'])
@auth.login_required
def files():
    req = request.get_json()
    files = None
    if request.method == 'POST':
        data = {
            'username': g.user.uName,
            'path': req['path'],
            'name': req['name'],
            'type': req['type']
        }

        url = f'http://{who_is_online()}:5001/dist/files'
        files = requests.post(url = url, json = data)
        
        typeOf = "file"
        if req['type'] == "dir":
            typeOf = "directory"
        
        op = Operation(name = f"Create new {typeOf} {req['name']}", typeOf = "create", user = g.user.id)
        db.session.add(op)
        db.session.commit()

    elif request.method == 'PUT':
        typeOf = "file"
        op = ""
        if req['operation'] == "rename":
            data = {
                'username': g.user.uName,
                'path': req['path'],
                'name': req['name'],
                'new': req['new'],
                'operation': req['operation']
            }

            url = f'http://{who_is_online()}:5001/dist/files'
            files = requests.put(url = url, json = data)

            if req['type'] == "dir":
                typeOf = "directory"
            
            op = Operation(name = f"Rename {typeOf} {req['name']} to {req['new']}", typeOf = req['operation'], user = g.user.id)
        elif req['operation'] == "modify":
            data = {
                'username': g.user.uName,
                'path': req['path'],
                'name': req['name'],
                'contents': req['contents'],
                'operation': req['operation']
            }

            url = f'http://{who_is_online()}:5001/dist/files'
            files = requests.put(url = url, json = data)

            op = Operation(name = f"Edit {typeOf} {req['name']}", typeOf = req['operation'], user = g.user.id)
        db.session.add(op)
        db.session.commit()
    elif request.method == 'DELETE':
        typeOf = "file"
        op = ""
        data = {
            'username': g.user.uName,
            'path': req['path'],
            'name': req['name'],
            'type': req['type']
        }

        url = f'http://{who_is_online()}:5001/dist/files'
        files = requests.delete(url = url, json = data)

        if req['type'] == "dir":
            typeOf = "directory"
        
        op = Operation(name = f"Delete {typeOf} {req['name']}", typeOf = "delete", user = g.user.id)
        db.session.add(op)
        db.session.commit()

    return jsonify(files.json())

@app.route('/api/files/history')
@auth.login_required
def get_file_history():
    ops = Operation.query.order_by(db.desc(Operation.trans_id)).filter_by(user = f'{g.user.id}').all()
    operations = []
    for op in ops :
        operations.append(op.as_dict())
    
    return jsonify(Status(True, operations).json())

def who_is_online():
    selected = None

    for host in hosts:
        try:
            socket.gethostbyaddr(host)
            selected = host
            break
        except socket.herror:
            print(f'{host} is offline')

    return selected

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    fName = db.Column(db.String(50), index = True)
    lName = db.Column(db.String(50), index = True)
    uName = db.Column(db.String(50), index = True)
    email = db.Column(db.String(120), index = True)
    phone = db.Column(db.String(20), index = True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return f'<User {self.uName}'

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)
    
    def generate_auth_token(self, expiration = 600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

    def update(self, data):
        for key, item in data.items():
            if key == 'password':
                self.password_hash = self.hash_password(item)
            setattr(self, key, item)
        
        db.session.commit()

class Operation(db.Model):
    __tablename__ = 'operations'
    trans_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(255), index = True)
    typeOf = db.Column(db.String(32), index = True)
    user = db.Column(db.String(32), index = True)
    # created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Operation {self.name}'
    
    def as_dict(self):
        return {
            "name": self.name,
            "typeOf": self.typeOf
        }

if __name__ == '__main__':
    app.secret_key = 'random_key'
    app.run(debug=True)