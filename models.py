from passlib.apps import custom_app_context as pwd_context
from app import db

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    fName = db.Column(db.String(50), index = True)
    lName = db.Column(db.String(50), index = True)
    uName = db.Column(db.String(50), index = True)
    email = db.Column(db.String(120), index = True)
    phone = db.Column(db.String(20), index = True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return f'<User {self.uname}'

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

class Operation(db.Model):
    __tablename__ = 'operations'
    trans_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(255), index = True)
    typeOf = db.Column(db.String(32), index = True)
    user = db.Column(db.String(32), index = True)

    def __repr__(self):
        return f'<Operation {self.name}'
